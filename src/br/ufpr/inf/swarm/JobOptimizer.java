/**
 * Swarm  
 */
package br.ufpr.inf.swarm;

import br.ufpr.inf.swarm.pso.PSOptimizer;

/**
 * @author Edson
 * 
 */
public class JobOptimizer {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PSOptimizer pso = new PSOptimizer();
		if (args.length < 2) {
			System.out.println("swarm [#iterations] [#particles]");
			System.exit(-1);
		}
		
		Integer iterations = Integer.parseInt(args[0]);
		Integer particles = Integer.parseInt(args[1]);
		pso.execute(iterations, particles);
		System.exit(0);
	}
}
