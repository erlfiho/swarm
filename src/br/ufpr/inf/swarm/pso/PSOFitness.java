/**
 * 
 */
package br.ufpr.inf.swarm.pso;

/**
 * @author Edson
 * 
 */
public class PSOFitness {
	private double current = Double.MAX_VALUE;
	private double best = Double.MAX_VALUE;

	public double calculate(PSOParticle.Position p, PSOParticle.Velocity v) {
		double x = p.getx();
		double y = p.gety();
		
		/* Objective function */
		current = Math.sin(Math.pow(x, 2) + Math.pow(y, 2));
		return current;
	}

	public boolean updateBestFitness() {
		if (Double.compare(current, best) < 0) {
			best = current;
			return true;
		}
		return false;
	}

	public double getBestFitness() {
		return best;
	}

	public double getCurrentFitness() {
		return current;
	}
}
