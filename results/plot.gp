#!/opt/local/bin/gnuplot
clear
reset
set xrange [-2:2]
set yrange [-2:2]
set zrange [-2:2]
set terminal gif animate delay 50
set output "animate.gif"
 splot sin(x**2+y**2) linecolor rgb "grey", "data0.txt" using 3:4:5 with points linecolor rgb "red"
 splot sin(x**2+y**2) linecolor rgb "grey", "data1.txt" using 3:4:5 with points linecolor rgb "red"
