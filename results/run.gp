#!/opt/local/bin/gnuplot
# set term postscript eps enhanced color
# set output 'output.eps'
# set pm3d
# splot "data.txt" using 3:4:5:1 with pm3d t "weaves", \
# 		"data.txt" using 3:4:5 with labels offset 1,0 point

# set view 130,20
# set xrange [-10:10]
# splot sin(sqrt(x**2+y**2))/sqrt(x**2+y**2)
# replot "data.txt" using 3:4:5 with points

#We will plot sin(x+t) in this gif animation
reset
set term gif animate
set output "animate.gif"
n=24    #n frames
i=0
load "animate.gnuplot"

